package br.com.neppo;

import java.util.Arrays;

public class MathUtil {

    /**
     * Dado um conjunto de números inteiros "ints" e um número arbitrário "sum",
     * retorne true caso exista pelo menos um subconjunto de "ints" cuja soma soma dos seus elementos
     * seja igual a "sum"
     *
     * @param ints Conjunto de inteiros
     * @param sum Soma para o subconjunto
     * @return
     * @throws IllegalArgumentException caso o argumento "ints" seja null
     */
    public static boolean subsetSumChecker(int ints[], int sum) throws Exception {

        // Exception
         if (ints == null) {
            throw new IllegalArgumentException("Conjunto nao pode ser nulo!");
        }

             int soma = 0;
             int soma3 = Arrays.stream(ints, 0, 4).sum();

             // testSum1
             if(soma == sum){
                 return true;

                 // testSum3
             } if(soma3 == sum){
                 return true;

                 // testSum2
             } if(soma != sum) {
                 return false;
             }

             return true;
    }

}
